#!/usr/bin/python3
# -*- coding: utf-8 -*-
import sys
from PySide2.QtWidgets import QMainWindow, QApplication, QGraphicsView, QGraphicsScene
from PySide2.QtCore import Qt, QTime, QTimer
from PySide2.QtGui import QPen, QBrush


class MainWindow(QMainWindow):

    def __init__(self):
        super(MainWindow, self).__init__()
        self.setWindowTitle("Binary clock")
        self.resize(250, 150)

        self.scene = QGraphicsScene()
        # self.scene.setSceneRect(0, 0, 400, 200)
        self.lgpen = QPen(Qt.lightGray)
        self.grpen = QPen(Qt.green)
        self.lgbrush = QBrush(Qt.lightGray)
        self.grbrush = QBrush(Qt.green)
        self.digitlist = []

        self.clock()
        self.timer = QTimer()
        self.timer.timeout.connect(self.binaryclock)
        self.timer.start(1000)

    def run(self, app):
        self.show()
        sys.exit(app.exec_())

    def clock(self):
        view = QGraphicsView(self.scene, self)
        view.setGeometry(0, 0, 250, 150)
        self.positions()

    def createdigit(self, pos_x, dmax=4):
        digitlist = []
        pos_y = 60
        if dmax != 4:
            pos_y = pos_y + ((4 - dmax) * 30)
        r = 20
        for x in range(dmax):
            digitlist.append(self.scene.addEllipse(pos_x, pos_y, r, r, self.lgpen, self.lgbrush))
            pos_y += 30
        return digitlist

    def positions(self):

        self.digitlist += self.createdigit(160, 2)
        self.digitlist += self.createdigit(190)
        self.digitlist += self.createdigit(240, 3)
        self.digitlist += self.createdigit(270)
        self.digitlist += self.createdigit(320, 3)
        self.digitlist += self.createdigit(350)

    def binaryclock(self):
        time = QTime.currentTime().toString()
        time = time.replace(":", "")
        h, hh, m, mm, s, ss = [time[i] for i in range(0, len(time))]
        ss = bin(int(ss))[2:].zfill(4)
        s = bin(int(s))[2:].zfill(3)
        mm = bin(int(mm))[2:].zfill(4)
        m = bin(int(m))[2:].zfill(3)
        hh = bin(int(hh))[2:].zfill(4)
        h = bin(int(h))[2:].zfill(2)

        binarytime = h + hh + m + mm + s + ss

        for digit in range(len(binarytime)):
            if int(binarytime[digit]) == 1:
                self.digitlist[digit].setPen(self.grpen)
                self.digitlist[digit].setBrush(self.grbrush)
            if int(binarytime[digit]) == 0:
                self.digitlist[digit].setPen(self.lgpen)
                self.digitlist[digit].setBrush(self.lgbrush)


def main():
    app = QApplication(sys.argv)
    MainWindow().run(app)


if __name__ == '__main__':
    main()
